package ro.utcn.converter.modelToDto;

import ro.utcn.converter.AbstractConverter;
import ro.utcn.dto.MonomialDTO;
import ro.utcn.model.MonomialModel;

public class MonomialModelToDtoConverter extends AbstractConverter<MonomialModel, MonomialDTO> {

    private static MonomialModelToDtoConverter monomialModelToDtoConverter;

    private MonomialModelToDtoConverter() {
    }

    protected void populate(MonomialModel source, MonomialDTO target) {
        final StringBuilder monomial = new StringBuilder();
        Double coefficient = source.getCoefficient();
        if (coefficient < 0) {
            monomial.append("-");
            coefficient *= -1;
        } else {
            monomial.append("+");
        }
        monomial.append(coefficient);
        long grade = source.getGrade();
        if (grade > 0) {
            monomial.append("x");
            if (grade > 1) {
                monomial.append("^");
                monomial.append(grade);
            }
        }
        target.setString(monomial.toString());
    }

    protected MonomialDTO createTarget() {
        return new MonomialDTO();
    }

    public static MonomialModelToDtoConverter getMonomialModelToDtoConverter() {
        if (monomialModelToDtoConverter == null) {
            monomialModelToDtoConverter = new MonomialModelToDtoConverter();
        }
        return monomialModelToDtoConverter;
    }
}
