package ro.utcn.converter.modelToDto;

import ro.utcn.converter.AbstractConverter;
import ro.utcn.dto.PolynomialDTO;
import ro.utcn.model.MonomialModel;
import ro.utcn.model.PolynomialModel;

import java.util.List;

public class PolynomialModelToDtoConverter extends AbstractConverter<PolynomialModel, PolynomialDTO> {

    private static PolynomialModelToDtoConverter polynomialModelToDtoConverter;

    private MonomialModelToDtoConverter monomialModelToDtoConverter = MonomialModelToDtoConverter.getMonomialModelToDtoConverter();

    private PolynomialModelToDtoConverter() {
    }

    protected void populate(PolynomialModel source, PolynomialDTO target) {
        final List<MonomialModel> monomials = source.getMonomials();
        final StringBuilder polynomial = new StringBuilder();
        for (MonomialModel monomial : monomials) {
            polynomial.append(getMonomialModelToDtoConverter().convert(monomial).getString());
        }
        target.setString(polynomial.toString());
    }

    protected PolynomialDTO createTarget() {
        return new PolynomialDTO();
    }

    public static PolynomialModelToDtoConverter getPolynomialModelToDtoConverter() {
        if (polynomialModelToDtoConverter == null) {
            polynomialModelToDtoConverter = new PolynomialModelToDtoConverter();
        }
        return polynomialModelToDtoConverter;
    }

    public MonomialModelToDtoConverter getMonomialModelToDtoConverter() {
        return monomialModelToDtoConverter;
    }
}
