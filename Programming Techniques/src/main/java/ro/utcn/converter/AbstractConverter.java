package ro.utcn.converter;

public abstract class AbstractConverter<S, T> implements Converter<S, T> {

    public T convert(S source) {
        if (source == null) return null;
        final T target = createTarget();
        populate(source, target);
        return target;
    }

    protected abstract void populate(S source, T target);

    protected abstract T createTarget();
}
