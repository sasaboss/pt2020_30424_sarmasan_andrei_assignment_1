package ro.utcn.converter.dtoToModel;

import ro.utcn.converter.AbstractConverter;
import ro.utcn.dto.MonomialDTO;
import ro.utcn.dto.PolynomialDTO;
import ro.utcn.model.MonomialModel;
import ro.utcn.model.PolynomialModel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolynomialDtoToModelConverter extends AbstractConverter<PolynomialDTO, PolynomialModel> {

    private MonomialDtoToModelConverter monomialDtoToModelConverter = MonomialDtoToModelConverter.getMonomialDtoToModel();

    private static PolynomialDtoToModelConverter polynomialDtoToModel;

    private PolynomialDtoToModelConverter() {
    }

    protected void populate(PolynomialDTO source, PolynomialModel target) {
        final Pattern regex = Pattern.compile("[+\\-]?\\d*([xX]((\\^\\d+)|(\\d?)))?");
        final Matcher matcher = regex.matcher(source.getString());
        final List<MonomialModel> monomials = new ArrayList<MonomialModel>();
        while (matcher.find()) {
            if(!matcher.group().equals("")) {
                final MonomialDTO monomialDTO = new MonomialDTO();
                monomialDTO.setString(matcher.group());
                final MonomialModel monomialModel = getMonomialDtoToModelConverter().convert(monomialDTO);
                monomials.add(monomialModel);
            }
        }
        target.setMonomials(monomials);
    }

    protected PolynomialModel createTarget() {
        return new PolynomialModel();
    }

    public static PolynomialDtoToModelConverter getPolynomialDtoToModel() {
        if (polynomialDtoToModel == null) {
            polynomialDtoToModel = new PolynomialDtoToModelConverter();
        }
        return polynomialDtoToModel;
    }

    public MonomialDtoToModelConverter getMonomialDtoToModelConverter() {
        return monomialDtoToModelConverter;
    }
}
