package ro.utcn.converter.dtoToModel;

import ro.utcn.converter.AbstractConverter;
import ro.utcn.dto.MonomialDTO;
import ro.utcn.model.MonomialModel;

public class MonomialDtoToModelConverter extends AbstractConverter<MonomialDTO, MonomialModel> {

    private static MonomialDtoToModelConverter monomialDtoToModel;

    private MonomialDtoToModelConverter() {
    }

    protected void populate(MonomialDTO source, MonomialModel target) {
        String sourceString = source.getString();
        int rootIndex = sourceString.indexOf("x");
        String rootSubstring;
        if (rootIndex == -1) {
            target.setGrade(0L);
            rootSubstring = sourceString;
        } else {
            int gradeIndex = sourceString.indexOf("^");
            if (gradeIndex != -1) target.setGrade(Long.valueOf(sourceString.substring(gradeIndex + 1)));
            else target.setGrade(1L);
            rootSubstring = sourceString.substring(0, rootIndex);
        }
        double coefficientValue = 1D;
        if (!rootSubstring.equals("")) {
            if (rootSubstring.charAt(0) == '+') rootSubstring = rootSubstring.substring(1);
            else if (rootSubstring.charAt(0) == '-') {
                rootSubstring = rootSubstring.substring(1);
                coefficientValue = -1D;
            }
            if (!rootSubstring.equals("")) coefficientValue = coefficientValue * Long.parseLong(rootSubstring);
        }
        target.setCoefficient(coefficientValue);
    }

    protected MonomialModel createTarget() {
        return new MonomialModel();
    }

    public static MonomialDtoToModelConverter getMonomialDtoToModel() {
        if (monomialDtoToModel == null) {
            monomialDtoToModel = new MonomialDtoToModelConverter();
        }
        return monomialDtoToModel;
    }
}
