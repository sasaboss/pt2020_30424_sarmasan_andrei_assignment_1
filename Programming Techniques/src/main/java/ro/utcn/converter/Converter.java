package ro.utcn.converter;

public interface Converter<S, T> {
    T convert(S source);
}