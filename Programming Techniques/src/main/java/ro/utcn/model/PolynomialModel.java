package ro.utcn.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PolynomialModel implements Serializable {

    private List<MonomialModel> monomials = new ArrayList<MonomialModel>();

    public List<MonomialModel> getMonomials() {
        return monomials;
    }

    public void setMonomials(List<MonomialModel> monomials) {
        this.monomials = monomials;
    }
}
