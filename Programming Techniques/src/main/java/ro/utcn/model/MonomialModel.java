package ro.utcn.model;

import java.io.Serializable;
import java.util.Objects;

public class MonomialModel implements Serializable {

    private Double coefficient;

    private Long grade;

    public Double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(Double coefficient) {
        this.coefficient = coefficient;
    }

    public Long getGrade() {
        return grade;
    }

    public void setGrade(Long grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonomialModel that = (MonomialModel) o;
        return Objects.equals(coefficient, that.coefficient) &&
                Objects.equals(grade, that.grade);
    }
}
