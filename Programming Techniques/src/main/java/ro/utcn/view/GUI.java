package ro.utcn.view;

import ro.utcn.controller.PolynomialController;
import ro.utcn.controller.impl.PolynomialControllerImpl;
import ro.utcn.dto.PolynomialDTO;
import ro.utcn.exception.PolynomialException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {

    private PolynomialController polynomialController = PolynomialControllerImpl.getPolynomialController();

    private GUI() {

        final JFrame frame = new JFrame("Polynomials");

        final JPanel operationPanel = new JPanel();
        final JLabel operationLabel = new JLabel();
        final JComboBox operationComboBox = new JComboBox();

        final JPanel firstPolynomialPanel = new JPanel();
        final JLabel firstPolynomialLabel = new JLabel("Polynomial 1");
        final JTextField firstPolynomialTextField = new JTextField();

        final JPanel secondPolynomialPanel = new JPanel();
        final JLabel secondPolynomialLabel = new JLabel("Polynomial 2");
        final JTextField secondPolynomialTextField = new JTextField();

        final JButton button = new JButton("Submit");

        final JLabel resultLabel = new JLabel();

        firstPolynomialPanel.add(firstPolynomialLabel);
        firstPolynomialPanel.add(firstPolynomialTextField);
        firstPolynomialPanel.setLayout(new GridLayout(5, 1));

        secondPolynomialPanel.add(secondPolynomialLabel);
        secondPolynomialPanel.add(secondPolynomialTextField);
        secondPolynomialPanel.setLayout(new GridLayout(5, 1));

        operationComboBox.addItem("Select Operation");
        operationComboBox.addItem("Addition");
        operationComboBox.addItem("Subtraction");
        operationComboBox.addItem("Multiplication");
        operationComboBox.addItem("Division");
        operationComboBox.addItem("Derivative");
        operationComboBox.addItem("Integration");

        operationPanel.add(operationLabel);
        operationPanel.add(operationComboBox);

        frame.add(operationPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(new GridLayout(5, 1));

        operationComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.remove(firstPolynomialPanel);
                frame.remove(secondPolynomialPanel);
                frame.remove(button);
                frame.remove(resultLabel);

                if (operationComboBox.getSelectedItem().equals("Addition") || operationComboBox.getSelectedItem().equals("Subtraction") ||
                        operationComboBox.getSelectedItem().equals("Multiplication") || operationComboBox.getSelectedItem().equals("Division")) {
                    frame.add(firstPolynomialPanel);
                    frame.add(secondPolynomialPanel);
                    frame.add(button);

                } else if (operationComboBox.getSelectedItem().equals("Derivative") || operationComboBox.getSelectedItem().equals("Integration")) {
                    frame.add(firstPolynomialPanel);
                    frame.add(button);
                }
                frame.repaint();
                frame.revalidate();
            }
        });

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (operationComboBox.getSelectedItem().equals("Addition")) {
                    final PolynomialDTO firstPolynomial = new PolynomialDTO();
                    firstPolynomial.setString(firstPolynomialTextField.getText().replaceAll(" ", "").toLowerCase());
                    final PolynomialDTO secondPolynomial = new PolynomialDTO();
                    secondPolynomial.setString(secondPolynomialTextField.getText().replaceAll(" ", "").toLowerCase());
                    String polynomialResult;
                    try {
                        PolynomialDTO result = getPolynomialController().addition(firstPolynomial, secondPolynomial);
                        polynomialResult = result.getString();
                    } catch (PolynomialException exception) {
                        polynomialResult = "Invalid Polynomial";
                    }
                    resultLabel.setText(polynomialResult);
                    frame.add(resultLabel);
                } else if (operationComboBox.getSelectedItem().equals("Subtraction")) {
                    final PolynomialDTO firstPolynomial = new PolynomialDTO();
                    firstPolynomial.setString(firstPolynomialTextField.getText().replaceAll(" ", "").toLowerCase());
                    final PolynomialDTO secondPolynomial = new PolynomialDTO();
                    secondPolynomial.setString(secondPolynomialTextField.getText().replaceAll(" ", "").toLowerCase());
                    String polynomialResult;
                    try {
                        PolynomialDTO result = getPolynomialController().subtraction(firstPolynomial, secondPolynomial);
                        polynomialResult = result.getString();
                    } catch (PolynomialException exception) {
                        polynomialResult = "Invalid Polynomial";
                    }
                    resultLabel.setText(polynomialResult);
                    frame.add(resultLabel);
                } else if (operationComboBox.getSelectedItem().equals("Multiplication")) {
                    final PolynomialDTO firstPolynomial = new PolynomialDTO();
                    firstPolynomial.setString(firstPolynomialTextField.getText().replaceAll(" ", "").toLowerCase());
                    final PolynomialDTO secondPolynomial = new PolynomialDTO();
                    String polynomialResult;
                    try {
                        PolynomialDTO result = getPolynomialController().multiplication(firstPolynomial, secondPolynomial);
                        polynomialResult = result.getString();
                    } catch (PolynomialException exception) {
                        polynomialResult = "Invalid Polynomial";
                    }
                    resultLabel.setText(polynomialResult);
                    frame.add(resultLabel);
                } else if (operationComboBox.getSelectedItem().equals("Derivative")) {
                    final PolynomialDTO firstPolynomial = new PolynomialDTO();
                    firstPolynomial.setString(firstPolynomialTextField.getText().replaceAll(" ", "").toLowerCase());
                    String polynomialResult;
                    try {
                        PolynomialDTO result = getPolynomialController().derivative(firstPolynomial);
                        polynomialResult = result.getString();
                    } catch (PolynomialException exception) {
                        polynomialResult = "Invalid Polynomial";
                    }
                    resultLabel.setText(polynomialResult);
                    frame.add(resultLabel);
                } else if (operationComboBox.getSelectedItem().equals("Integration")) {
                    final PolynomialDTO firstPolynomial = new PolynomialDTO();
                    firstPolynomial.setString(firstPolynomialTextField.getText().replaceAll(" ", "").toLowerCase());
                    String polynomialResult;
                    try {
                        PolynomialDTO result = getPolynomialController().integration(firstPolynomial);
                        polynomialResult = result.getString();
                    } catch (PolynomialException exception) {
                        polynomialResult = "Invalid Polynomial";
                    }
                    resultLabel.setText(polynomialResult);
                    frame.add(resultLabel);
                }
                frame.repaint();
                frame.revalidate();
            }
        });
    }

    public static void main(String[] args) {
        final GUI gui = new GUI();
    }

    public PolynomialController getPolynomialController() {
        return polynomialController;
    }
}