package ro.utcn.dto;

public class PolynomialDTO {

    private String string;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
