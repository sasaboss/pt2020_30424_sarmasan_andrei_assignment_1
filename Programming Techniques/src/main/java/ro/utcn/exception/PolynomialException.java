package ro.utcn.exception;

public class PolynomialException extends RuntimeException {
    public PolynomialException(final String message) {
        super(message);
    }
}
