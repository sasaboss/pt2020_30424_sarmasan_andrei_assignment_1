package ro.utcn.controller;

import ro.utcn.dto.PolynomialDTO;

public interface PolynomialController {
    PolynomialDTO addition(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial);
    PolynomialDTO subtraction(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial);
    PolynomialDTO multiplication(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial);
    PolynomialDTO derivative(final PolynomialDTO firstPolynomial);
    PolynomialDTO integration(final PolynomialDTO firstPolynomial);
}
