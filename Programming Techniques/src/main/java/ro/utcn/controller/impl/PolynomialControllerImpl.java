package ro.utcn.controller.impl;

import ro.utcn.controller.PolynomialController;
import ro.utcn.dto.PolynomialDTO;
import ro.utcn.exception.PolynomialException;
import ro.utcn.facade.PolynomialFacade;
import ro.utcn.facade.impl.PolynomialFacadeImpl;
import ro.utcn.validator.Validator;
import ro.utcn.validator.impl.PolynomialValidator;

public class PolynomialControllerImpl implements PolynomialController {

    private PolynomialFacade polynomialFacade = PolynomialFacadeImpl.getPolynomialFacade();

    private Validator<PolynomialDTO> polynomialDTOValidator = PolynomialValidator.getPolynomialValidator();

    private static PolynomialControllerImpl polynomialController;

    private PolynomialControllerImpl() {
    }

    public PolynomialDTO addition(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial) {
        if(getPolynomialDTOValidator().validate(firstPolynomial) && getPolynomialDTOValidator().validate(firstPolynomial)) {
            return getPolynomialFacade().addition(firstPolynomial, secondPolynomial);
        } else {
            throw new PolynomialException("Invalid polynomial");
        }
    }

    public PolynomialDTO subtraction(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial) {
        if(getPolynomialDTOValidator().validate(firstPolynomial) && getPolynomialDTOValidator().validate(firstPolynomial)) {
            return getPolynomialFacade().subtraction(firstPolynomial, secondPolynomial);
        } else {
            throw new PolynomialException("Invalid polynomial");
        }
    }

    public PolynomialDTO multiplication(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial) {
        if(getPolynomialDTOValidator().validate(firstPolynomial) && getPolynomialDTOValidator().validate(firstPolynomial)) {
            return getPolynomialFacade().multiplication(firstPolynomial, secondPolynomial);
        } else {
            throw new PolynomialException("Invalid polynomial");
        }
    }

    public PolynomialDTO derivative(final PolynomialDTO firstPolynomial) {
        if(getPolynomialDTOValidator().validate(firstPolynomial)) {
            return getPolynomialFacade().derivative(firstPolynomial);
        } else {
            throw new PolynomialException("Invalid polynomial");
        }
    }

    public PolynomialDTO integration(final PolynomialDTO firstPolynomial) {
        if(getPolynomialDTOValidator().validate(firstPolynomial)) {
            return getPolynomialFacade().integration(firstPolynomial);
        } else {
            throw new PolynomialException("Invalid polynomial");
        }
    }

    public static PolynomialControllerImpl getPolynomialController() {
        if(polynomialController == null) {
            polynomialController = new PolynomialControllerImpl();
        }
        return polynomialController;
    }

    public PolynomialFacade getPolynomialFacade() {
        return polynomialFacade;
    }

    public Validator<PolynomialDTO> getPolynomialDTOValidator() {
        return polynomialDTOValidator;
    }
}
