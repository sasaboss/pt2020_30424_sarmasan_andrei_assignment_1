package ro.utcn.validator;

public interface Validator<DTO> {
    boolean validate(final DTO dto);
}