package ro.utcn.validator.impl;

import ro.utcn.dto.PolynomialDTO;
import ro.utcn.validator.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PolynomialValidator implements Validator<PolynomialDTO> {

    private static PolynomialValidator polynomialValidator;

    private PolynomialValidator() {
    }

    public boolean validate(PolynomialDTO dto) {
        final Pattern regex = Pattern.compile("(^[+\\-]?\\d*([xX]((\\^\\d+)|(\\d?)))?)([+\\-]\\d*([xX]((\\^\\d+)|(\\d?)))?)*$");
        final Matcher matcher = regex.matcher(dto.getString());
        return matcher.find();
    }

    public static PolynomialValidator getPolynomialValidator() {
        if(polynomialValidator == null) {
            polynomialValidator = new PolynomialValidator();
        }
        return polynomialValidator;
    }
}
