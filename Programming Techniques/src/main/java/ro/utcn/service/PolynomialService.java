package ro.utcn.service;

import ro.utcn.model.PolynomialModel;

public interface PolynomialService {
    PolynomialModel addition(final PolynomialModel firstPolynomial, final PolynomialModel secondPolynomial);
    PolynomialModel subtraction(final PolynomialModel firstPolynomial, final PolynomialModel secondPolynomial);
    PolynomialModel multiplication(final PolynomialModel firstPolynomial, final PolynomialModel secondPolynomial);
    PolynomialModel derivative(final PolynomialModel polynomial);
    PolynomialModel integration(final PolynomialModel polynomial);
}
