package ro.utcn.service.impl;

import ro.utcn.model.MonomialModel;
import ro.utcn.model.PolynomialModel;
import ro.utcn.service.PolynomialService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PolynomialServiceImpl implements PolynomialService {

    private static PolynomialService polynomialService;

    public PolynomialModel addition(final PolynomialModel firstPolynomial, final PolynomialModel secondPolynomial) {
        final HashMap<Long, Double> map = new HashMap<>();
        for (MonomialModel monomial : firstPolynomial.getMonomials()) {
            if (map.containsKey(monomial.getGrade())) {
                map.put(monomial.getGrade(), monomial.getCoefficient() + map.get(monomial.getGrade()));
            } else {
                map.put(monomial.getGrade(), monomial.getCoefficient());
            }
        }
        for (MonomialModel monomial : secondPolynomial.getMonomials()) {
            if (map.containsKey(monomial.getGrade())) {
                map.put(monomial.getGrade(), monomial.getCoefficient() + map.get(monomial.getGrade()));
            } else {
                map.put(monomial.getGrade(), monomial.getCoefficient());
            }
        }
        return mapToPolynomial(map);
    }

    public PolynomialModel subtraction(final PolynomialModel firstPolynomial, final PolynomialModel secondPolynomial) {
        final HashMap<Long, Double> map = new HashMap<>();
        for (MonomialModel monomial : firstPolynomial.getMonomials()) {
            if (map.containsKey(monomial.getGrade())) {
                map.put(monomial.getGrade(), monomial.getCoefficient() + map.get(monomial.getGrade()));
            } else {
                map.put(monomial.getGrade(), monomial.getCoefficient());
            }
        }
        for (MonomialModel monomial : secondPolynomial.getMonomials()) {
            if (map.containsKey(monomial.getGrade())) {
                map.put(monomial.getGrade(), -monomial.getCoefficient() + map.get(monomial.getGrade()));
            } else {
                map.put(monomial.getGrade(), -monomial.getCoefficient());
            }
        }
        return mapToPolynomial(map);
    }

    public PolynomialModel multiplication(final PolynomialModel firstPolynomial, final PolynomialModel secondPolynomial) {
        final HashMap<Long, Double> map = new HashMap<>();
        for (MonomialModel firstMonomial : firstPolynomial.getMonomials()) {
            for (MonomialModel secondMonomial : secondPolynomial.getMonomials()) {
                long grade = firstMonomial.getGrade() + secondMonomial.getGrade();
                double coefficient = firstMonomial.getCoefficient() * secondMonomial.getCoefficient();
                if (map.containsKey(grade)) {
                    map.put(grade, coefficient + map.get(grade));
                } else {
                    map.put(grade, coefficient);
                }
            }
        }
        return mapToPolynomial(map);
    }

    public PolynomialModel derivative(final PolynomialModel polynomial) {
        final HashMap<Long, Double> map = new HashMap<>();
        for (MonomialModel monomial : polynomial.getMonomials()) {
            long grade = monomial.getGrade();
            double coefficient = monomial.getCoefficient();
            if (grade != 0) {
                if (map.containsKey(grade - 1)) {
                    map.put(grade - 1, coefficient * grade + map.get(grade - 1));
                } else {
                    map.put(grade - 1, coefficient * grade);
                }
            }
        }
        return mapToPolynomial(map);
    }

    public PolynomialModel integration(final PolynomialModel polynomial) {
        final HashMap<Long, Double> map = new HashMap<>();
        for (MonomialModel monomial : polynomial.getMonomials()) {
            long grade = monomial.getGrade() + 1;
            double coefficient = monomial.getCoefficient();
            if (map.containsKey(grade)) {
                map.put(grade, coefficient * 1D / grade + map.get(grade));
            } else {
                map.put(grade, coefficient * 1D / grade);
            }
        }
        return mapToPolynomial(map);
    }

    private PolynomialModel mapToPolynomial(final HashMap<Long, Double> map) {
        final PolynomialModel result = new PolynomialModel();
        final List<MonomialModel> monomials = new ArrayList<>();
        for (Long entryKey : map.keySet()) {
            final MonomialModel monomial = new MonomialModel();
            monomial.setGrade(entryKey);
            monomial.setCoefficient(map.get(entryKey));
            monomials.add(monomial);
        }
        result.setMonomials(monomials);
        return result;
    }

    private PolynomialServiceImpl() {
    }

    public static PolynomialService getPolynomialService() {
        if (polynomialService == null) {
            polynomialService = new PolynomialServiceImpl();
        }
        return polynomialService;
    }

}
