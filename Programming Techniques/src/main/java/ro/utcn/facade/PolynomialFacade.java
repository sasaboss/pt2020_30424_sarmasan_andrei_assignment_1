package ro.utcn.facade;

import ro.utcn.dto.PolynomialDTO;

public interface PolynomialFacade {
    PolynomialDTO addition(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial);
    PolynomialDTO subtraction(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial);
    PolynomialDTO multiplication(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial);
    PolynomialDTO derivative(final PolynomialDTO polynomial);
    PolynomialDTO integration(final PolynomialDTO polynomial);
}
