package ro.utcn.facade.impl;

import ro.utcn.converter.dtoToModel.PolynomialDtoToModelConverter;
import ro.utcn.converter.modelToDto.PolynomialModelToDtoConverter;
import ro.utcn.dto.PolynomialDTO;
import ro.utcn.facade.PolynomialFacade;
import ro.utcn.model.PolynomialModel;
import ro.utcn.service.PolynomialService;
import ro.utcn.service.impl.PolynomialServiceImpl;

public class PolynomialFacadeImpl implements PolynomialFacade {

    private PolynomialService polynomialService = PolynomialServiceImpl.getPolynomialService();

    private PolynomialModelToDtoConverter polynomialModelToDtoConverter = PolynomialModelToDtoConverter.getPolynomialModelToDtoConverter();

    private PolynomialDtoToModelConverter polynomialDtoToModelConverter = PolynomialDtoToModelConverter.getPolynomialDtoToModel();

    private static PolynomialFacade polynomialFacade;

    private PolynomialFacadeImpl() {
    }

    public PolynomialDTO addition(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial) {
        final PolynomialModel firstPolynomialModel = getDtoToModelConverter().convert(firstPolynomial);
        final PolynomialModel secondPolynomialModel = getDtoToModelConverter().convert(secondPolynomial);
        final PolynomialModel result = getPolynomialService().addition(firstPolynomialModel, secondPolynomialModel);
        return getModelToDtoConverter().convert(result);
    }

    public PolynomialDTO subtraction(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial) {
        final PolynomialModel firstPolynomialModel = getDtoToModelConverter().convert(firstPolynomial);
        final PolynomialModel secondPolynomialModel = getDtoToModelConverter().convert(secondPolynomial);
        final PolynomialModel result = getPolynomialService().subtraction(firstPolynomialModel, secondPolynomialModel);
        return getModelToDtoConverter().convert(result);
    }

    public PolynomialDTO multiplication(final PolynomialDTO firstPolynomial, final PolynomialDTO secondPolynomial) {
        final PolynomialModel firstPolynomialModel = getDtoToModelConverter().convert(firstPolynomial);
        final PolynomialModel secondPolynomialModel = getDtoToModelConverter().convert(secondPolynomial);
        final PolynomialModel result = getPolynomialService().multiplication(firstPolynomialModel, secondPolynomialModel);
        return getModelToDtoConverter().convert(result);
    }

    public PolynomialDTO derivative(final PolynomialDTO polynomial) {
        final PolynomialModel polynomialModel = getDtoToModelConverter().convert(polynomial);
        final PolynomialModel result = getPolynomialService().derivative(polynomialModel);
        return getModelToDtoConverter().convert(result);
    }

    public PolynomialDTO integration(final PolynomialDTO polynomial) {
        final PolynomialModel polynomialModel = getDtoToModelConverter().convert(polynomial);
        final PolynomialModel result = getPolynomialService().integration(polynomialModel);
        return getModelToDtoConverter().convert(result);
    }

    public static PolynomialFacade getPolynomialFacade() {
        if(polynomialFacade == null) {
            polynomialFacade = new PolynomialFacadeImpl();
        }
        return polynomialFacade;
    }

    public PolynomialService getPolynomialService() {
        return polynomialService;
    }

    public PolynomialModelToDtoConverter getModelToDtoConverter() {
        return polynomialModelToDtoConverter;
    }

    public PolynomialDtoToModelConverter getDtoToModelConverter() {
        return polynomialDtoToModelConverter;
    }
}
