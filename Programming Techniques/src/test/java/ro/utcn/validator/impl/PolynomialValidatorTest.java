package ro.utcn.validator.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ro.utcn.dto.PolynomialDTO;

import static org.junit.Assert.*;

public class PolynomialValidatorTest {

    private PolynomialValidator polynomialValidator;

    @Before
    public void setUp() throws Exception {
        polynomialValidator = PolynomialValidator.getPolynomialValidator();
    }

    @Test
    public void validate_ValidPolynomial() {
        final PolynomialDTO polynomialDTO = new PolynomialDTO();
        polynomialDTO.setString("10x^10+x-x^3");
        assertTrue(polynomialValidator.validate(polynomialDTO));
    }

    @Test
    public void validate_InvalidPolynomial() {
        final PolynomialDTO polynomialDTO = new PolynomialDTO();
        polynomialDTO.setString("10x^10+x-x^3 - 1.1x");
        assertFalse(polynomialValidator.validate(polynomialDTO));
    }

    @After
    public void tearDown() throws Exception {
        polynomialValidator = null;
    }
}