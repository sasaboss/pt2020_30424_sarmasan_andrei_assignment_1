package ro.utcn.service.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ro.utcn.model.MonomialModel;
import ro.utcn.model.PolynomialModel;
import ro.utcn.service.PolynomialService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PolynomialServiceImplTest {

    private PolynomialService polynomialService;

    @Before
    public void setUp() throws Exception {
        polynomialService = PolynomialServiceImpl.getPolynomialService();
    }

    @Test
    public void addition_Success() {
        final PolynomialModel firstPolynomial = new PolynomialModel();
        final PolynomialModel secondPolynomial = new PolynomialModel();
        List<MonomialModel> monomials = new ArrayList<>();
        MonomialModel monomialModel = new MonomialModel();
        monomialModel.setGrade(1L);
        monomialModel.setCoefficient(10D);
        monomials.add(monomialModel);
        firstPolynomial.setMonomials(monomials);
        secondPolynomial.setMonomials(monomials);
        final PolynomialModel expectedResult = new PolynomialModel();
        monomialModel = new MonomialModel();
        monomialModel.setCoefficient(20D);
        monomialModel.setGrade(1L);
        monomials = new ArrayList<>();
        monomials.add(monomialModel);
        expectedResult.setMonomials(monomials);
        final PolynomialModel result = polynomialService.addition(firstPolynomial, secondPolynomial);

        assertTrue(result.getMonomials().containsAll(expectedResult.getMonomials()));
    }

    @Test
    public void subtraction_Success() {
        final PolynomialModel firstPolynomial = new PolynomialModel();
        final PolynomialModel secondPolynomial = new PolynomialModel();
        List<MonomialModel> monomials = new ArrayList<>();
        MonomialModel monomialModel = new MonomialModel();
        monomialModel.setGrade(1L);
        monomialModel.setCoefficient(20D);
        monomials.add(monomialModel);
        firstPolynomial.setMonomials(monomials);
        monomials = new ArrayList<>();
        monomialModel = new MonomialModel();
        monomialModel.setGrade(1L);
        monomialModel.setCoefficient(10D);
        monomials.add(monomialModel);
        secondPolynomial.setMonomials(monomials);
        final PolynomialModel expectedResult = new PolynomialModel();
        monomialModel = new MonomialModel();
        monomialModel.setCoefficient(10D);
        monomialModel.setGrade(1L);
        monomials = new ArrayList<>();
        monomials.add(monomialModel);
        expectedResult.setMonomials(monomials);
        final PolynomialModel result = polynomialService.subtraction(firstPolynomial, secondPolynomial);

        assertTrue(result.getMonomials().containsAll(expectedResult.getMonomials()));
    }

    @Test
    public void multiplication_Success() {
        final PolynomialModel firstPolynomial = new PolynomialModel();
        final PolynomialModel secondPolynomial = new PolynomialModel();
        List<MonomialModel> monomials = new ArrayList<>();
        MonomialModel monomialModel = new MonomialModel();
        monomialModel.setGrade(1L);
        monomialModel.setCoefficient(10D);
        monomials.add(monomialModel);
        firstPolynomial.setMonomials(monomials);
        secondPolynomial.setMonomials(monomials);
        final PolynomialModel expectedResult = new PolynomialModel();
        monomialModel = new MonomialModel();
        monomialModel.setCoefficient(100D);
        monomialModel.setGrade(2L);
        monomials = new ArrayList<>();
        monomials.add(monomialModel);
        expectedResult.setMonomials(monomials);
        final PolynomialModel result = polynomialService.multiplication(firstPolynomial, secondPolynomial);

        assertTrue(result.getMonomials().containsAll(expectedResult.getMonomials()));
    }

    @Test
    public void derivative_Success() {
        final PolynomialModel firstPolynomial = new PolynomialModel();
        List<MonomialModel> monomials = new ArrayList<>();
        MonomialModel monomialModel = new MonomialModel();
        monomialModel.setGrade(2L);
        monomialModel.setCoefficient(10D);
        monomials.add(monomialModel);
        firstPolynomial.setMonomials(monomials);
        final PolynomialModel expectedResult = new PolynomialModel();
        monomialModel = new MonomialModel();
        monomialModel.setCoefficient(20D);
        monomialModel.setGrade(1L);
        monomials = new ArrayList<>();
        monomials.add(monomialModel);
        expectedResult.setMonomials(monomials);
        final PolynomialModel result = polynomialService.derivative(firstPolynomial);

        assertTrue(result.getMonomials().containsAll(expectedResult.getMonomials()));
    }

    @Test
    public void integration_Success() {
        final PolynomialModel firstPolynomial = new PolynomialModel();
        List<MonomialModel> monomials = new ArrayList<>();
        MonomialModel monomialModel = new MonomialModel();
        monomialModel.setGrade(2L);
        monomialModel.setCoefficient(12D);
        monomials.add(monomialModel);
        firstPolynomial.setMonomials(monomials);
        final PolynomialModel expectedResult = new PolynomialModel();
        monomialModel = new MonomialModel();
        monomialModel.setCoefficient(4D);
        monomialModel.setGrade(3L);
        monomials = new ArrayList<>();
        monomials.add(monomialModel);
        expectedResult.setMonomials(monomials);
        final PolynomialModel result = polynomialService.integration(firstPolynomial);

        assertTrue(result.getMonomials().containsAll(expectedResult.getMonomials()));
    }

    @After
    public void tearDown() throws Exception {
        polynomialService = null;
    }
}